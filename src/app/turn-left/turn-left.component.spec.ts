import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TurnLeftComponent } from './turn-left.component';

describe('TurnLeftComponent', () => {
  let component: TurnLeftComponent;
  let fixture: ComponentFixture<TurnLeftComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TurnLeftComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TurnLeftComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
