import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core'; 

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { NavbarComponent } from './navbar/navbar.component';
import { TakeSelfieComponent } from './take-selfie/take-selfie.component';
import { PharmacyComponent } from './pharmacy/pharmacy.component';
import { BookConsultingComponent } from './book-consulting/book-consulting.component';
import { BookingFailedComponent } from './booking-failed/booking-failed.component';
import { FreeReportComponent } from './free-report/free-report.component';
import { FreeReport2Component } from './free-report2/free-report2.component';
import { FontFaceComponent } from './font-face/font-face.component';
import { GetPremiumReportComponent } from './get-premium-report/get-premium-report.component';
import { LoginComponent } from './login/login.component';
import { MyAccountComponent } from './my-account/my-account.component';
import { OrderFailedComponent } from './order-failed/order-failed.component';
import { OrderSuccessComponent } from './order-success/order-success.component';
import { OtpComponent } from './otp/otp.component';
import { PreparingTaskComponent } from './preparing-task/preparing-task.component';
import { ProductDescriptionComponent } from './product-description/product-description.component';
import { Screen8Component } from './screen8/screen8.component';
import { TurnLeftComponent } from './turn-left/turn-left.component';
import { TurnRightComponent } from './turn-right/turn-right.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    TakeSelfieComponent,
    PharmacyComponent,
    BookConsultingComponent,
    BookingFailedComponent,
    FreeReportComponent,
    FreeReport2Component,
    FontFaceComponent,
    GetPremiumReportComponent,
    LoginComponent,
    MyAccountComponent,
    OrderFailedComponent,
    OrderSuccessComponent,
    OtpComponent,
    PreparingTaskComponent,
    ProductDescriptionComponent,
    Screen8Component,
    TurnLeftComponent,
    TurnRightComponent,
    
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent},
      { path: 'take-selfie', component: TakeSelfieComponent },
      { path: 'pharmacy', component: PharmacyComponent },
      { path: 'book-consulting', component: BookConsultingComponent },
      { path: 'booking-failed', component: BookingFailedComponent },
      { path: 'free-report', component: FreeReportComponent },
      { path: 'free-report2', component: FreeReport2Component },
      { path: 'font-face', component: FontFaceComponent },
      { path: 'get-premium-report', component: GetPremiumReportComponent },
      { path: 'login', component: LoginComponent },
      { path: 'my-account', component: MyAccountComponent },
      { path: 'order-failed', component: OrderFailedComponent },
      { path: 'order-success', component: OrderSuccessComponent },
      { path: 'otp', component: OtpComponent },
      { path: 'preparing-task', component: PreparingTaskComponent },
      { path: 'product-description', component: ProductDescriptionComponent },
      { path: 'screen8', component: Screen8Component },
      { path: 'turn-left', component: TurnLeftComponent },
      { path: 'turn-right', component: TurnRightComponent },
    ]),
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
