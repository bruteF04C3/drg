import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FreeReport2Component } from './free-report2.component';

describe('FreeReport2Component', () => {
  let component: FreeReport2Component;
  let fixture: ComponentFixture<FreeReport2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FreeReport2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FreeReport2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
