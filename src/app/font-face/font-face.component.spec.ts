import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FontFaceComponent } from './font-face.component';

describe('FontFaceComponent', () => {
  let component: FontFaceComponent;
  let fixture: ComponentFixture<FontFaceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FontFaceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FontFaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
