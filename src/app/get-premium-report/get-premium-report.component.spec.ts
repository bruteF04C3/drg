import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetPremiumReportComponent } from './get-premium-report.component';

describe('GetPremiumReportComponent', () => {
  let component: GetPremiumReportComponent;
  let fixture: ComponentFixture<GetPremiumReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetPremiumReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetPremiumReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
