import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreparingTaskComponent } from './preparing-task.component';

describe('PreparingTaskComponent', () => {
  let component: PreparingTaskComponent;
  let fixture: ComponentFixture<PreparingTaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreparingTaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreparingTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
