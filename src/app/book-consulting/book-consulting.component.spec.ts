import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookConsultingComponent } from './book-consulting.component';

describe('BookConsultingComponent', () => {
  let component: BookConsultingComponent;
  let fixture: ComponentFixture<BookConsultingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookConsultingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookConsultingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
