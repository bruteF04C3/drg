import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TurnRightComponent } from './turn-right.component';

describe('TurnRightComponent', () => {
  let component: TurnRightComponent;
  let fixture: ComponentFixture<TurnRightComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TurnRightComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TurnRightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
